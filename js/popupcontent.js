function volunteerStringGeneration(){
  var volunteerReturnString = '<select class="form-control" id="submit_form_volunteer" name="volunteer">';
  CRM.$.ajaxSetup({async:false});
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "contact_sub_type": "Volunteer"
  }).done(function(result) {
    // do something
    for(var i=0; i < result.count; i++){
      volunteerReturnString += '<option value="' + result.values[i].first_name + " " + result.values[i].last_name + '"' + 'data-id=' + result.values[i].id + '>' + result.values[i].first_name + " " + result.values[i].last_name +'</option>';
    }
  });
  return volunteerReturnString + '</select>';
}

function languageStringGeneration(){
  var languageReturnString = '<select class="form-control" id="submit_form_language" name="language">';
  // var languageset = [];
  // CRM.api3('Contact', 'get', {
  //   "sequential": 1,
  //   "return": ["preferred_language"],
  //   "contact_type": "Individual",
  //   "options": {"limit":1000000}
  // }).done(function(result) {
  //   // do something
  //   for(var i=0; i< result.count; i++){
  //     if(languageset.includes(result.values[i].preferred_language)){
  //       continue;
  //     }
  //     else{
  //       if(result.values[i].preferred_language===""){
  //         languageset.push("");
  //         continue;
  //       }
  //       else{
  //         languageReturnString +='<option value="' + result.values[i].preferred_language +'">' + result.values[i].preferred_language + '</option>';
  //         languageset.push(result.values[i].preferred_language);
  //       }
  //     }
  //   }
  // });
  languageReturnString += '<option value="' + "English" +'">' + "English" + '</option>';
  languageReturnString += '<option value="' + "Spanish" +'">' + "Spanish" + '</option>';
  languageReturnString += '<option value="' + "French" +'">' + "French" + '</option>';
  return languageReturnString + '</select>';
}

function timeAssignedForStringGeneration(){
  var time_string = '<input type="date" placeholder="Required" class="form-control" id="submit_form_date" name="submit_form_date"/>';
  time_string += '<input type="time"/>';
  return time_string;
}



function generateAvailableSurveys(){
  var stringForReturn = '<select class="form-control" id="submit_existing_survey" name="existing_survey">';
  CRM.$.ajaxSetup({async: false}); //NOTE TO SELF! Get rid of this line and replace with ajax promises
  CRM.api3('Survey', 'get', {
    "sequential": 1
  }).done(function(result) {
    for(var i = 0; i<result.values.length; i++){
      stringForReturn += '<option value="' + result.values[i].title + '" data-id = "' + result.values[i].id + '"  >'+ result.values[i].title + '</option>';
    }
  });
  return stringForReturn + '</select>';
}

function custom_group_names(){
  var stringForReturn = '<input type="text" id="submit-custom-group" name="uname" maxlength="30" placeholder="Optional name of group." />';
  return stringForReturn;
}

function draw_another_shape(){
  popupGroup.eachLayer(function(layer){
    popupGroup.removeLayer(layer);
  });
}

function generic_popup_content(centerlatlng){
   var popup = '<form role="form" id="form" enctype="multipart/form-data" class = "form-horizontal" method="post">'+
       '<div class="form-group">'+
       // '<div><img src="images/walking-solid.svg"><i class="fa fa-bicycle" aria-hidden="true"></i><i class="fa fa-car" aria-hidden="true"></i></div>' +
    '<label class="control-label col-sm-5"><strong>Volunteer: </strong></label>'+
    volunteerStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Language: </strong></label>'+
    languageStringGeneration() +
    '</div>'+
    '<div class="form-group">'+
    '<label class="control-label col-sm-5"><strong>Time assigned for: </strong></label>'+
    timeAssignedForStringGeneration()
    +
    '</div>'+
    '<div class="form-group">' +
    '<label class="control-label col-sm-5"><strong> Use Existing Survey: </strong></label>'+
    generateAvailableSurveys() +
       '</div>' +
       '<div class="form-group">'+
       '<label class="control-label col-sm-5"><strong>Custom group name (optional): </strong></label>'+
       custom_group_names() +
       '</div>' +
    '<input style="display: none;" type="text" id="lat" name="lat" value="'+centerlatlng.lat.toFixed(6)+'" />'+
    '<input style="display: none;" type="text" id="lng" name="lng" value="'+centerlatlng.lng.toFixed(6)+'" />'+
        '<div class="form-group">'+
        '<div style="text-align:center; margin-top:10px;" class="col-xs-4"><button type="button" value="draw another shape" class="btn btn-primary" onclick="draw_another_shape()" id="draw_another_shape_form">Draw Another Shape</button></div>';
    return popup;
}

function get_popupcontentPolygon(centerlatlng,voterIDsWithinPolygon){
  window.voterIDsWithinPolygon = voterIDsWithinPolygon; //Here we use a global variable for the namespace of the html
  var popupcontentPolygon = generic_popup_content(centerlatlng);
  // popupcontentPolygon += '<div style="text-align:center; margin-top:10px" class="col-xs-4"><button type="button" value="submit" class="btn btn-primary" onclick="survey_handler_for_polygon(this)" id="submit_polygon_form">Submit</button></div>'+ '</div>'+ '</form>';
  popupcontentPolygon += '<div><div style="text-align:center; margin-top:10px" class="col-xs-4"><button type="button" value="submit" class="btn btn-primary" onclick="save_handler_for_polygon(this)" id="submit_polygon_form" style="margin-right: 80px;">Save voters as group</button><button type="button" value="submit" class="btn btn-primary" onclick="survey_handler_for_polygon(this)" id="submit_polygon_form">Create and assign turf</button></div></div>' + '</div>' + '</form>';
    return popupcontentPolygon;
}

function save_handler_for_polygon(element){
  var voterIDsWithinPolygon = window.voterIDsWithinPolygon;
  delete window.voterIDsWithinPolygon;
  var new_group_name_string = "";
  //Check if we have a custom group name
  if(document.getElementById("submit-custom-group").value != ""){
    new_group_name_string += document.getElementById("submit-custom-group").value;
  }
  else{
    var date = getDateTime(); // Function is in civiapi.js file
    new_group_name_string += "Group created at time" + "_" + date.replace(/ /g,"_");
  }
  //Create group
  var result = CRM.api3('Group', 'create', {
    "title": new_group_name_string
  }).done(function(result) {
    // Assign voters to group
    CRM.api3('GroupContact', 'create', {
      "group_id": result.id,
      "contact_id": voterIDsWithinPolygon
    });
  });
  var result_is_error = result.responseJSON.is_error;
  //Cleanup by deleting any leftover artifacts
  delete_popup_layers();
  delete_drawnItems_layers();
  //Add a popup to confirm that the group has been created if group was created
  if(result_is_error == 0){
    var popup = L.popup({
      closeButton: true,
      autoClose: true,
      maxWidth : 500
    })
      .setLatLng(map.getBounds().getCenter())
      .setContent('<p> Contact group has been created with name  ' + new_group_name_string + '</p>')
        .openOn(map);
  }
  //if group was not created successfully then display an error popup
  else{
    var popup = L.popup({
      closeButton: true,
      autoClose: true,
      maxWidth : 500
    })
        .setLatLng(map.getBounds().getCenter())
        .setContent('<p style="color:red;"> Contact group was not created. Error message: ' + result.responseJSON.error_message + '</p>')
      .openOn(map);
  }
}

function survey_handler_for_polygon(element){
  var voterIDsWithinPolygon = window.voterIDsWithinPolygon;
  delete window.voterIDsWithinPolygon;
  var volunteerValue = document.getElementById("submit_form_volunteer").value;
  for(var i=0; i<document.getElementById("submit_form_volunteer").length; i++){
    var value = document.getElementById("submit_form_volunteer").children[i].value;
    if(value == volunteerValue){
      var volunteerID = document.getElementById("submit_form_volunteer").children[i].dataset.id;
    }
  }
  // var languageValue = document.getElementById("submit_form_language").value;
  var custom_group_name = document.getElementById("submit-custom-group").value;
    var dateValue = document.getElementById("submit_form_date").value;
  var surveytitle = document.getElementById("submit_existing_survey").value;
  var surveyTemp = document.getElementById("submit_existing_survey").options.selectedIndex;
    var surveyID = document.getElementById("submit_existing_survey").options[surveyTemp].dataset.id;
  var new_group_name_string = add_voters_to_survey(surveyID,volunteerID,voterIDsWithinPolygon,custom_group_name);
  //We now delete the popup element and also delete the polygon
  delete_popup_layers();
  delete_drawnItems_layers();
  //We will now show a message to confirm that the survey has been created
  var popup = L.popup({
    closeButton: true,
    autoClose: true,
    maxWidth : 500
  })
      .setLatLng(map.getBounds().getCenter())
      .setContent('<p>Survey has been created. The contact group is called ' + new_group_name_string + '</p>')
    .openOn(map);
}

function get_popupcontentRectangle(centerlatlng,voterIDsWithinRectangle){
  window.voterIDsWithinRectangle = voterIDsWithinRectangle;
    var popupcontentRectangle =  generic_popup_content(centerlatlng);
  popupcontentRectangle += '<div style="text-align:center; margin-top:10px;" class="col-xs-4"><button type="button" value="submit" class="btn btn-primary" onclick="save_handler_for_rectangle(this)" id="submit_polygon_form" style="margin-right: 80px;">Save voters as group</button><button type="button" value="submit" class="btn btn-primary" onclick="survey_handler_for_rectangle(this)" id="submit_rectangle_form">Create and assign turf</button></div>'+ '</div>'+ '</form>';
    return popupcontentRectangle;
}

function save_handler_for_rectangle(element){
  var voterIDsWithinRectangle = window.voterIDsWithinRectangle;
  delete window.voterIDsWithinRectangle;
  var new_group_name_string = "";
  //Check if we have a custom group name
  if(document.getElementById("submit-custom-group").value != ""){
    new_group_name_string += document.getElementById("submit-custom-group").value;
  }
  else{
    var date = getDateTime(); // Function is in civiapi.js file
    new_group_name_string += "Group created at time" + "_" + date.replace(/ /g,"_");
  }
  //Create group
  var result = CRM.api3('Group', 'create', {
    "title": new_group_name_string
  }).done(function(result) {
    // Assign voters to group
    CRM.api3('GroupContact', 'create', {
      "group_id": result.id,
      "contact_id": voterIDsWithinRectangle
    });
  });
  var result_is_error = result.responseJSON.is_error;
  //Cleanup by deleting any leftover artifacts
  delete_popup_layers();
  delete_drawnItems_layers();
  //Add a popup to confirm that the group has been created if group was created
  if(result_is_error == 0){
    var popup = L.popup({
      closeButton: true,
      autoClose: true,
      maxWidth : 500
    })
      .setLatLng(map.getBounds().getCenter())
      .setContent('<p> Contact group has been created with name  ' + new_group_name_string + '</p>')
        .openOn(map);
  }
  //if group was not created successfully then display an error popup
  else{
    var popup = L.popup({
      closeButton: true,
      autoClose: true,
      maxWidth : 500
    })
        .setLatLng(map.getBounds().getCenter())
        .setContent('<p style="color:red;"> Contact group was not created. Error message: ' + result.responseJSON.error_message + '</p>')
      .openOn(map);
  }
}


function survey_handler_for_rectangle(element){
  var voterIDsWithinRectangle = window.voterIDsWithinRectangle;
  delete window.voterIDsWithinRectangle;
  var custom_group_name = document.getElementById("submit-custom-group").value;
  var volunteerValue = document.getElementById("submit_form_volunteer").value;
  var volunteerID = document.getElementById("submit_form_volunteer").lastChild.dataset.id;
  // var languageValue = document.getElementById("submit_form_language").value;
  var dateValue = document.getElementById("submit_form_date").value;
  var surveytitle = document.getElementById("submit_existing_survey").value;
  var surveyID = document.getElementById("submit_existing_survey").lastChild.dataset.id;
  add_voters_to_survey(surveyID,volunteerID,voterIDsWithinRectangle,custom_group_name);
  //delete popup content from the map on click
  delete_popup_layers();
  delete_drawnItems_layers();
}



function get_popupcontentCircle(centerlatlng,voterIDsWithinCircle){
  window.voterIDsWithinCircle = voterIDsWithinCircle;
    var popupcontentCircle =  generic_popup_content(centerlatlng);
  popupcontentCircle += '<div style="text-align:center; margin-top:10px;" class="col-xs-4"><button type="button" value="submit" class="btn btn-primary" onclick="save_handler_for_circle(this)" id="submit_polygon_form" style="margin-right: 80px;">Save voters as group</button><button type="button" value="submit" class="btn btn-primary" onclick="survey_handler_for_circle(this)" id="submit_circle_form">Create and assign turf</button></div>'+
    '</div>'+ '</form>';
    return popupcontentCircle;

}

function save_handler_for_circle(element){
  var voterIDsWithinCircle = window.voterIDsWithinCircle;
  delete window.voterIDsWithinCircle;
  var new_group_name_string = "";
  //Check if we have a custom group name
  if(document.getElementById("submit-custom-group").value != ""){
    new_group_name_string += document.getElementById("submit-custom-group").value;
  }
  else{
    var date = getDateTime(); // Function is in civiapi.js file
    new_group_name_string += "Group created at time" + "_" + date.replace(/ /g,"_");
  }
  //Create group
  var result = CRM.api3('Group', 'create', {
    "title": new_group_name_string
  }).done(function(result) {
    // Assign voters to group
    CRM.api3('GroupContact', 'create', {
      "group_id": result.id,
      "contact_id": voterIDsWithinCircle
    });
  });
  var result_is_error = result.responseJSON.is_error;
  //Cleanup by deleting any leftover artifacts
  delete_popup_layers();
  delete_drawnItems_layers();
  //Add a popup to confirm that the group has been created if group was created
  if(result_is_error == 0){
    var popup = L.popup({
      closeButton: true,
      autoClose: true,
      maxWidth : 500
    })
      .setLatLng(map.getBounds().getCenter())
      .setContent('<p> Contact group has been created with name  ' + new_group_name_string + '</p>')
        .openOn(map);
  }
  //if group was not created successfully then display an error popup
  else{
    var popup = L.popup({
      closeButton: true,
      autoClose: true,
      maxWidth : 500
    })
        .setLatLng(map.getBounds().getCenter())
        .setContent('<p style="color:red;"> Contact group was not created. Error message: ' + result.responseJSON.error_message + '</p>')
      .openOn(map);
  }
}


function survey_handler_for_circle(element){
  var voterIDsWithinCircle = window.voterIDsWithinCircle;
  delete window.voterIDsWithinCircle;
  var custom_group_name = document.getElementById("submit-custom-group").value;
  var volunteerValue = document.getElementById("submit_form_volunteer").value;
  var volunteerID = document.getElementById("submit_form_volunteer").lastChild.dataset.id;
  // var languageValue = document.getElementById("submit_form_language").value;
  var dateValue = document.getElementById("submit_form_date").value;
  var surveytitle = document.getElementById("submit_existing_survey").value;
  var surveyID = document.getElementById("submit_existing_survey").lastChild.dataset.id;
  add_voters_to_survey(surveyID,volunteerID,voterIDsWithinCircle,custom_group_name);
  //delete popup content from map
  delete_popup_layers();
  delete_drawnItems_layers();
}

function delete_popup_layers(){
  popupGroup.eachLayer(function(layer){
    popupGroup.removeLayer(layer);
  });
}

function delete_drawnItems_layers(){
  drawnItems.eachLayer(function(layer){
    drawnItems.removeLayer(layer);
  });
}
