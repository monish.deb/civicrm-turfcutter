<?php
use CRM_Turfcutter_ExtensionUtil as E;

/**
 * SurveyRespondant.Create API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_survey_respondant_Create_spec(&$spec) {
  $spec['survey_id']['api.required'] = 1;
  $spec['interviewer_id']['api.required'] = 1;
  $spec['respondent_ids']['api.required'] = 1;
}

/**
 * SurveyRespondant.Create API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_survey_respondant_Create($params) {
  // get mandatory params
  $surveyID = $params['survey_id'];
  $interviewerID = $params['interviewer_id'];
  $respondentIDs = $params['respondent_ids'];
  $surveyDetails = civicrm_api3('Survey', 'getsingle', ['id' => $surveyID]);
  // I am expecting here a array of contact IDs, if this API is called via JS then it must be passed into JSON string/via form submission then array of contact IDs
  $respondentIDs =  $params['respondent_ids'];
  // get group ID or name of new group
  if (!empty($params['group_id'])) {
    $groupID = $params['group_id'];
  }
  elseif (!empty($params['new_group_name'])) {
    // create group via API, using group name
    $groupID = civicrm_api3('Group', 'create', ['title' => $params['new_group_name']])['id'];
  }
  // iterate for each contact
  foreach ($respondentIDs as $contactID) {
  // assign to group if group ID is present
    if (!empty($groupID)) {
    // see GroupContact.create API how to add contacts to a group
      civicrm_api3('GroupContact', 'create', ['group_id' => $groupID,'contact_id' => $contactID]);
    }
    // create activity to reserve each respondant
    $result = civicrm_api3('Activity', 'create', [
                  'source_contact_id' => 'user_contact_id',
                  'assignee_id' => $interviewerID,
                  'target_contact_id' => array($contactID),
                  'source_record_id' => $surveyID,
                  'activity_type_id' => $surveyDetails['activity_type_id'],
                  'subject' => $surveyDetails['title'] . ' - ' . ts('Respondent Reservation'),
                  'status_id' => "Scheduled",
                  'campaign_id' => CRM_Utils_Array::value('campaign_id', $surveyDetails),
    ]);
}


  if (true) {
    $returnValues = $result;
    return civicrm_api3_create_success($returnValues, $params, 'NewEntity', 'NewAction');
  }
  else {
    throw new API_Exception(/*errorMessage*/ 'Everyone knows that the magicword is "sesame"', /*errorCode*/ 1234);
  }
}
